<?php

/**
 * @file
 * Admin callbacks for the exact_target_soap module.
 */

/**
 * Returns the system settings form.
 */
function exact_target_soap_settings() {
  $form = array();

  // Prevent displaying this warning more than once. The warning message is
  // incorrectly displayed if a batch was just processed -- probably the
  // message gets stored in the queue and not unset before the page is refreshed,
  // or the static variable cache doesn't update before the page is loaded.
  $messages = drupal_get_messages('warning', TRUE);
  if (isset($messages['warning']) && is_array($messages['warning']) && sizeof($messages['warning'])) {
    foreach ($messages['warning'] as $warning) {
      if (strpos($warning, 'Your site is queueing ExactTarget requests.') === 0) {
        continue;
      }
      drupal_set_message($warning, 'warning');
    }
  }

  // Provide information about the state of the batch queue.
  if (variable_get('exact_target_soap_batch', FALSE)) {
    $count = db_result(db_query("SELECT COUNT(*) FROM {exact_target_soap_batch}"));
    $msg = 'Your site is queueing ExactTarget requests. The batch queue currently contains !count.';
    $args = array(
      '!count' => format_plural($count, 'one API request', '@count API requests'),
    );
    if ($count > 0) {
      $msg .= '  You should clear the batch as soon as possible.';
    }
    drupal_set_message(t($msg, $args), 'warning');
  }

  $form['batch'] = array(
    '#type' => 'fieldset',
    '#title' => t('Batch processing'),
    '#description' => t('Processes all queued ExactTarget API requests. Note: It is generally better practice to clear the ExactTarget queue using the included drush command: exact-target-soap-clear-batch (etc).'),
  );
  $form['batch']['clear'] = array(
    '#type' => 'submit',
    '#value' => t('Clear'),
    '#submit' => array('exact_target_soap_admin_clear_batch'),
  );

  $form['api'] = array(
    '#type' => 'fieldset',
    '#title' => t('API settings'),
  );
  $form['api']['exact_target_soap_wsdl'] = array(
    '#type' => 'textfield',
    '#title' => t('ExactTarget API WSDL'),
    '#default_value' => variable_get('exact_target_soap_wsdl', ''),
    '#description' => t('Path to the ExactTarget API WSDL endpoint.'),
    '#required' => TRUE,
  );
  $form['api']['exact_target_soap_username'] = array(
    '#type' => 'textfield',
    '#title' => t('ExactTarget API user'),
    '#default_value' => variable_get('exact_target_soap_username', ''),
    '#description' => t('The user name that is used to access the ExactTarget SOAP API.'),
  );
  $form['api']['exact_target_soap_password'] = array(
    '#type' => 'password',
    '#title' => t('ExactTarget API password'),
    '#default_value' => variable_get('exact_target_soap_password', ''),
    '#description' => t('The password that is used to access the ExactTarget SOAP API.'),
  );
  $form['api']['exact_target_soap_synchronous_api'] = array(
    '#type' => 'radios',
    '#title' => t('ExactTarget API Synchronous Communication'),
    '#options' => array(
      EXACT_TARGET_SOAP_ASYNCHRONOUS => t('Use Asynchronous Communication (normal setting)'),
      EXACT_TARGET_SOAP_SYNCHRONOUS => t('Use Synchronous Communication (better debugging)'),
    ),
    '#default_value' => variable_get('exact_target_soap_synchronous_api', EXACT_TARGET_SOAP_ASYNCHRONOUS),
    '#description' => t('Sets the communication with Exact Target into synchronous or asynchronous mode.  Asyncronous mode is more reliable and preferred, synchronous mode produces better debugging messages if there are problems.'),
  );
  $form['api']['exact_target_soap_verbose'] = array(
    '#type' => 'checkbox',
    '#title' => t('Verbose logging'),
    '#default_value' => variable_get('exact_target_soap_verbose', FALSE),
    '#description' => t('Enables logging of types of requests and the time to fly for successful, retry, and failed requests.'),
  );
  $form['api']['exact_target_soap_allow_retry'] = array(
    '#type' => 'checkbox',
    '#title' => t('Retry failed requests'),
    '#default_value' => variable_get('exact_target_soap_allow_retry', TRUE),
    '#description' => t('Retries failed requests within a given time threshold.'),
  );
  if (variable_get('exact_target_soap_allow_retry', TRUE)) {
    $form['api']['exact_target_soap_retry'] = array(
      '#type' => 'textfield',
      '#title' => t('Retry timeout threshold'),
      '#default_value' => variable_get('exact_target_soap_retry', 2.5),
      '#description' => t('Length of time in seconds to allow failed API requets to retry.'),
    );
  }

  $form['cleanup'] = array(
    '#type' => 'fieldset',
    '#title' => t('Batch cleanup'),
  );
  $form['cleanup']['exact_target_soap_cleanup'] = array(
    '#type' => 'radios',
    '#title' => t('Batch cleanup options'),
    '#options' => array(
      EXACT_TARGET_SOAP_CLEANUP_NOTHING => t('Do nothing'),
      EXACT_TARGET_SOAP_CLEANUP_MAIL => t('Send email notification'),
      EXACT_TARGET_SOAP_CLEANUP_CRON => t('Clean on cron'),
    ),
    '#default_value' => variable_get('exact_target_soap_cleanup', EXACT_TARGET_SOAP_CLEANUP_NOTHING),
    '#description' => t('Action to take if the system goes into batch mode.'),
  );
  $form['cleanup']['exact_target_soap_mail'] = array(
    '#type' => 'textfield',
    '#title' => t('Notification email(s)'),
    '#default_value' => variable_get('exact_target_soap_mail', variable_get('site_mail', '')),
    '#description' => t('Enter the email addresses (comma separated) that you would like to notify if the system goes into batch mode.'),
  );

  return system_settings_form($form);
}

/**
 * Submit callback for Clear button on settings page.
 */
function exact_target_soap_admin_clear_batch($form, &$form_state) {
  exact_target_soap_clear_batch(TRUE);
}

/**
 * Returns the list of queued SOAP requests.
 */
function exact_target_soap_queue() {
  $rows = array();
  $limit = 50;
  $header = array(
    array(
      'data' => t('ID'),
      'field' => 'bid',
      'sort' => 'asc',
    ),
    array(
      'data' => t('Operation'),
      'field' => 'method',
    ),
    t('Parameters'),
    array(
      'data' => t('Date queued'),
      'field' => 'queued',
    ),
    t('Options')
  );
  $res = pager_query("SELECT bid, method, params, queued FROM {exact_target_soap_batch} " . tablesort_sql($header), $limit);
  while ($row = db_fetch_array($res)) {
    $row['params'] = truncate_utf8($row['params'], 50, FALSE, TRUE);
    $row['queued'] = format_date($row['queued']);
    $links = array(
      'view' => array(
        'title' => 'view',
        'href' => 'admin/settings/exact_target_soap/queue/' . $row['bid'],
      ),
      'delete' => array(
        'title' => 'delete',
        'href' => 'admin/settings/exact_target_soap/queue/' . $row['bid'] . '/delete',
      ),
    );
    $row['options'] = theme('links', $links);
    $rows[] = $row;
  }

  return theme('table', $header, $rows) . theme('pager', array(), $limit);
}

/**
 * Returns information about a SOAP request in the batch queue.
 */
function exact_target_soap_queue_request($bid) {
  $request = db_fetch_array(db_query("SELECT * FROM {exact_target_soap_batch} WHERE bid=%d", $bid));
  if (!$request) {
    return drupal_not_found();
  }

  foreach ($request as $k => &$v) {
    if ($k == 'queued') {
      $v = format_date($v);
    }
    $v = $k . ': ' . $v;
  }

  return theme('item_list', $request);
}

/**
 * Returns a form to delete a SOAP request from the batch queue.
 */
function exact_target_soap_queue_request_delete($form_state, $bid) {
  $request = db_fetch_array(db_query("SELECT * FROM {exact_target_soap_batch} WHERE bid=%d", $bid));

  if (!$request) {
    return drupal_not_found();
  }

  $form['request'] = array(
    '#type' => 'value',
    '#value' => $request,
  );
  $form = confirm_form(
    $form,
    t('Would you like to delete this request?'),
    'admin/settings/exact_target_soap/queue',
    t('This action cannot be undone.'),
    t('Delete')
  );

  return $form;
}

function exact_target_soap_queue_request_delete_submit($form, &$form_state) {
  if (db_query("DELETE FROM {exact_target_soap_batch} WHERE bid=%d", $form_state['values']['request']['bid'])) {
    drupal_set_message(t('The SOAP request has been deleted.'));
    $form_state['redirect'] = 'admin/settings/exact_target_soap/queue';
    return;
  }
  drupal_set_message(t('There was a problem deleting the SOAP request.'), 'error');
}

