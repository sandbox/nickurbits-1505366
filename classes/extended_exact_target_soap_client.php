<?php

/**
 * @file
 * This class provides missing API Objects from the WSDL.
 */

class ExactTarget_DataFolder {
  public $ParentFolder;
  public $Name;
  public $Description;
  public $ContentType;
  public $IsActive;
  public $IsEditable;
  public $AllowChildren;
}

