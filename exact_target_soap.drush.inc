<?php

/**
 * @file
 * Drush callbacks for the exact_target_soap module.
 */

/**
 * Implements hook_drush_command().
 */
function exact_target_soap_drush_command() {
  $items = array();

  $items['exact-target-soap-clear-batch'] = array(
    'description' => dt('Processes all queued ExactTarget SOAP requests.'),
    'options' => array(
      'timeout' => dt('Number of seconds the runtime of the job should be limited to.'),
    ),
    'aliases' => array('etc'),
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 */
function exact_target_soap_drush_help($section) {
  switch ($section) {
    case 'drush:exact-target-soap-clear-batch':
      return dt('Process all queued ExactTarget SOAP requests and disable batch processing if the API is stable.');
  }
}

/**
 * Drush callback for clearing the ExactTarget batch queue.
 */
function drush_exact_target_soap_clear_batch() {
  exact_target_soap_clear_batch(FALSE, drush_get_option('timeout', 0));
}

/**
 * Release the lock if command execution halted.
 */
function drush_exact_target_soap_clear_batch_rollback() {
  lock_release('exact_target_soap_batch_semaphore');
}

